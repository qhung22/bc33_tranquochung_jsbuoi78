function getElement(n) {
  return document.getElementById(n);
}
//kiểm tra SNT
function soNT(n) {
  var dem = 0;
  for (var i = 0; i <= n; i++) {
    if (n % i == 0) {
      dem++;
    }
  }
  if (dem == 2) {
    var ketQua = n;
  }
  return ketQua;
}
// Đếm Số Dương
function demSoDuong(arr) {
  var dem = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      dem++;
    }
    return dem;
  }
}
//đếm số Âm
function demSoAm(arr) {
  for (dem = 0, i = 0; i < arr.length; i++) {
    if (arr[i] < 0) {
      dem++;
    }
    return dem;
  }
}
var numArray = [];
function getNumber() {
  var n = getElement("inputNum").value * 1;
  numArray.push(n);
  getElement("txtArray").innerHTML = numArray;
}
function sumPositive() {
  for (var sum = 0, i = 0; i < numArray.length; i++)
    if (numArray[i] > 0) {
      sum += numArray[i];
    }
  getElement("txtSum").innerText = `Tổng số Dương: ${sum}`;
}
function countPositive() {
  for (var dem = 0, i = 0; i < numArray.length; i++) if (numArray[i] > 0) dem++;
  getElement("txtCount").innerText = `Đếm Số Dương: ${dem}`;
}
function findMin() {
  for (var min = numArray[0], i = 0; i < numArray.length; i++)
    if (min > numArray[i]) min = numArray[i];
  getElement("txtMin").innerText = `Số nhỏ nhât : ${min}`;
}
function findMinPos() {
  var n = numArray.sort();
  for (var min = 0, i = 0; i < n.length; i++) {
    if (n[i] >= 0) {
      min = n[i];
      break;
    }
  }
  getElement("txtMinPos").innerHTML = `Số Dương nhỏ nhất : ${min}`;
}
function findEven() {
  for (var soChanLast = 0, i = 0; i < numArray.length; i++)
    if (numArray[i] % 2 == 0 && (soChanLast = numArray[i])) {
      getElement("txtEven").innerHTML = `Số chẵn cuối cùng: ${soChanLast}`;
    } else {
      getElement("txtEven").innerHTML = `không có số chẵn trong mảng`;
    }
}
function changePosition() {
  var viTri1 = getElement("inputIndex1").value * 1;
  var viTri2 = getElement("inputIndex2").value * 1;
  var newArr = numArray.slice(0);
  var bienTam = newArr.slice(viTri1, viTri1 + 1);
  newArr[viTri2] = newArr[viTri1];
  newArr[viTri2] = bienTam;
  getElement("txtChangePos").innerHTML = newArr;
}
function sortIncrease() {
  var sapXepTangDan = numArray.sort(function (a, b) {
    return a - b;
  });
  getElement("txtIncrease").innerHTML = sapXepTangDan;
}
function txtIncrease() {
  for (var i = 0; i < numArray.length; i++) {
    if (numArray[i] == soNT(numArray[i])) {
      ketQua = numArray[i];
      break;
    } else {
      ketQua = "Không có số nguyên tố";
    }
  }

  getElement("txtPrime").innerHTML = ketQua;
}
function findInt() {
  for (var dem = 0, i = 0; i < numArray.length; i++) {
    if (Number.isInteger(numArray[i])) {
      dem++;
    } else {
    }
  }
  getElement("txtInt").innerHTML = dem;
}
function compareNum() {
  var tongSoDuong = demSoDuong(numArray);
  var tongSoAm = demSoAm(numArray);
  var result = "";
  if (tongSoAm > tongSoDuong) {
    result = "Số Âm > Số Dương";
  } else {
    result = "Số Âm < Số Dương";
  }
  getElement("txtCompare").innerHTML = result;
}
